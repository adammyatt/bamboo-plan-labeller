package com.pronetbeans.bamboo.planlabeller;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.labels.LabelManager;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.task.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * Bamboo Task for Un-Labelling Plans.
 *
 * @author Adam Myatt
 */
public class UnLabelPlanTask implements TaskType {

    private static final Logger log = Logger.getLogger(UnLabelPlanTask.class);
    private PlanManager planManager;
    private LabelManager labelManager;

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException {

        final BuildLogger buildLogger = taskContext.getBuildLogger();

        final String useTextfieldLabels = taskContext.getConfigurationMap().get("useTextfieldLabels");
        final String labels = taskContext.getConfigurationMap().get("labels");
        final String useBuildFileLabels = taskContext.getConfigurationMap().get("useBuildFileLabels");
        final String localFileName = taskContext.getConfigurationMap().get("localFileName");

        boolean itWorked = false;

        try {
            String planKey = taskContext.getBuildContext().getParentBuildContext().getPlanKey();
            buildLogger.addBuildLogEntry("Preparing to remove Labels from Plan : " + planKey);
            Plan plan = planManager.getPlanByKey(planKey);

            if (useTextfieldLabels != null && "true".equals(useTextfieldLabels) && labels != null && labels.trim().length() > 0) {

                String[] allLabels = labels.trim().split(",");
                for (int i = 0; i < allLabels.length; i++) {
                    String lab = allLabels[i];
                    try {

                        labelManager.removeLabel(lab, plan.getPlanKey(), null);
                        buildLogger.addBuildLogEntry("Removed Label : " + lab);
                    } catch (Exception e) {
                        buildLogger.addBuildLogEntry("Error removing label ''" + lab + " from plan. Error details : " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            }

            if (useBuildFileLabels != null && "true".equals(useBuildFileLabels) && localFileName != null && localFileName.trim().length() > 0) {

                File inputFile = null;
                BufferedReader CSVFile = null;
                try {
                    inputFile = new File(taskContext.getWorkingDirectory(), localFileName);

                    if (inputFile.exists()) {

                        CSVFile = new BufferedReader(new FileReader(inputFile));
                        String dataRow = CSVFile.readLine();

                        while (dataRow != null) {
                            String[] dataArray = dataRow.split(",");
                            for (String item : dataArray) {

                                try {
                                    labelManager.removeLabel(item, plan.getPlanKey(), null);
                                    buildLogger.addBuildLogEntry("Removing Label : " + item);
                                } catch (Exception e) {
                                    buildLogger.addBuildLogEntry("Error removing label ''" + item + " from plan. Error details : " + e.getMessage());
                                    e.printStackTrace();
                                }
                            }

                            dataRow = CSVFile.readLine(); // Read next line of data.
                        }


                    } else {
                        buildLogger.addBuildLogEntry("ERROR. Specified file (" + localFileName + ") does not exist in the working directory for this build.");
                    }

                } catch (Exception e) {
                    buildLogger.addBuildLogEntry("Error removing labels from file : " + e.getMessage());
                    e.printStackTrace();
                } finally {
                    try {
                        // Close the file once all data has been read.
                        if (CSVFile != null) {
                            CSVFile.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            itWorked = true;

            log.info(Utils.getLogBanner());

        } catch (Exception e) {
            buildLogger.addBuildLogEntry("Error : " + e.getMessage());
            e.printStackTrace();
        } finally {

            try {
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (itWorked) {
            return TaskResultBuilder.create(taskContext).success().build();
        } else {
            return TaskResultBuilder.create(taskContext).failedWithError().build();
        }
    }

    /**
     * @return the planManager
     */
    public PlanManager getPlanManager() {
        return planManager;
    }

    /**
     * @param planManager the planManager to set
     */
    public void setPlanManager(PlanManager planManager) {
        this.planManager = planManager;
    }

    /**
     * @return the labelManager
     */
    public LabelManager getLabelManager() {
        return labelManager;
    }

    /**
     * @param labelManager the labelManager to set
     */
    public void setLabelManager(LabelManager labelManager) {
        this.labelManager = labelManager;
    }
}
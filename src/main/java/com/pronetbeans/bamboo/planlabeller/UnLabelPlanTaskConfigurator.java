package com.pronetbeans.bamboo.planlabeller;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Class for configuring parameters used by Task class for un-labelling plans.
 *
 * @author Adam Myatt
 */
public class UnLabelPlanTaskConfigurator extends AbstractTaskConfigurator {

    private TextProvider textProvider;

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("useTextfieldLabels", params.getString("useTextfieldLabels"));
        config.put("labels", params.getString("labels"));
        config.put("useBuildFileLabels", params.getString("useBuildFileLabels"));
        config.put("localFileName", params.getString("localFileName"));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context) {
        super.populateContextForCreate(context);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        context.put("useTextfieldLabels", taskDefinition.getConfiguration().get("useTextfieldLabels"));
        context.put("labels", taskDefinition.getConfiguration().get("labels"));
        context.put("useBuildFileLabels", taskDefinition.getConfiguration().get("useBuildFileLabels"));
        context.put("localFileName", taskDefinition.getConfiguration().get("localFileName"));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        context.put("useTextfieldLabels", taskDefinition.getConfiguration().get("useTextfieldLabels"));
        context.put("labels", taskDefinition.getConfiguration().get("labels"));
        context.put("useBuildFileLabels", taskDefinition.getConfiguration().get("useBuildFileLabels"));
        context.put("localFileName", taskDefinition.getConfiguration().get("localFileName"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        final String useTextfieldLabels = params.getString("useTextfieldLabels");
        final String labels = params.getString("labels");
        final String useBuildFileLabels = params.getString("useBuildFileLabels");
        final String localFileName = params.getString("localFileName");

        if (StringUtils.isNotEmpty(useTextfieldLabels)) {
            if (StringUtils.isEmpty(labels)) {
                errorCollection.addError("labels", textProvider.getText("com.pronetbeans.bamboo.planlabeller.UnLabelPlanTask.error"));
            }
        }
        if (StringUtils.isNotEmpty(useBuildFileLabels)) {
            if (StringUtils.isEmpty(localFileName)) {
                errorCollection.addError("localFileName", textProvider.getText("com.pronetbeans.bamboo.planlabeller.UnLabelPlanTask.error"));
            }
        }
    }

    public void setTextProvider(final TextProvider textProvider) {
        this.textProvider = textProvider;
    }
}

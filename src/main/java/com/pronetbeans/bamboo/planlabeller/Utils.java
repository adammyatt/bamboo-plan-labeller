package com.pronetbeans.bamboo.planlabeller;

/**
 * Utilities class.
 *
 * @author Adam Myatt
 */
public class Utils {

    public static String getLogBanner() {
        StringBuilder sb = new StringBuilder(1000);

        sb.append('\n');

        for (String s : Constants.banner) {
            sb.append(s);
            sb.append('\n');
        }

        return sb.toString();
    }
}


[@ui.bambooSection title='Remove Label(s) from Plan' ]

    [@ww.checkbox labelKey="Use Specified Labels" name="useTextfieldLabels" toggle='true'/]
    [@ww.textfield labelKey="Labels" name="labels" required='false' description="Provide a comma-delimited list of labels."/]
    <br><br>
    [@ww.checkbox labelKey="Use Labels in a File in your Build" name="useBuildFileLabels" toggle='true'/]
    [@ww.textfield labelKey="File Name" name="localFileName" required='false' description="Specify a file that is relative to your build's working directory."/]
[/@ui.bambooSection]


<br>
<br>





[@ui.bambooSection title='Remove Label(s) from Plan' ]
    [@ww.label labelKey="Use Specified Labels" name="useTextfieldLabels"/]
    [@ww.label labelKey="Labels" name="labels"/]
    <br><br>
    [@ww.label labelKey="Use Labels in a File in your Build" name="useBuildFileLabels"/]
    [@ww.label labelKey="File Name" name="localFileName"/]
[/@ui.bambooSection]